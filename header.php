<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<script src="ajax.js" type="text/javascript"></script>
<link rel="stylesheet" href="design1.css">
<link rel="stylesheet" href="design2.css">
<link rel="stylesheet" href="analysis.css">
<title> Examination Portal</title>
	<script>
		function timer(start, duration){
		var now=new Date()
		var nowhours=now.getHours()
		var nowminutes=now.getMinutes()
		var nowseconds=now.getSeconds()
		var durationhours=duration.getHours()
		var durationminutes=duration.getMinutes()
		var durationseconds=duration.getSeconds()
		var starthours=start.getHours()
		var startminutes=start.getMinutes()
		var startseconds=start.getSeconds()
		var hours=nowhours - starthours
		var minutes=nowminutes - startminutes
		var seconds=nowseconds - startseconds	
		if (minutes<0)
		{
			minutes=60+minutes
			hours = -1+hours
		}
		if (seconds<0)
		{
			seconds=60+seconds
			minutes=-1+minutes
		}
		
		
		if (minutes<=9)
		minutes="0"+minutes
		if (seconds<=9)
		seconds="0"+seconds
		document.Tick.Clock.value=hours+":"+minutes+":"
		+seconds
		if (seconds==durationseconds && minutes ==durationminutes && hours ==durationhours )
		 window.location.assign("displayScore.php")
		setTimeout("timer(start, duration)",1000)
		}
		
		
	</script>
	<script src="JS.js"></script>
	<script> $(function() {
    $("#confirmPassword").keyup(function() {
        var password = $("#password").val();
        $("#divCheckPasswordMatch").html(password == $(this).val() ? "<font color='green'>Passwords match</font>" : "Passwords do not match!");
    });

});</script>

<script type="text/javascript">
	
$(document).ready(function()//When the dom is ready 
{
$("#username").change(function() 
{ //if theres a change in the username textbox

var username = $("#username").val();//Get the value in the username textbox
if(username.length > 3)//if the lenght greater than 3 characters
{
$("#availability_status").html('<img src="loader.gif" align="absmiddle">&nbsp;Checking availability...');
//Add a loading image in the span id="availability_status"


$.ajax({  //Make the Ajax Request
    type: "POST",  
    url: "ajax_check_username.php",  //file name
    data: "username="+ username,  //data
    success: function(server_response){  
 
	if(server_response == '0')//if ajax_check_username.php return value "0"
	{ 
	$("#availability_status").html('<img src="available.png" align="absmiddle"> <font color="Green"> Available </font>  ');
	//add this image to the span with id "availability_status"
	}  
	else  if(server_response == '1')//if it returns "1"
	{  
	 $("#availability_status").html('<img src="not_available.png" align="absmiddle"> <font color="red">Not Available </font>');
	}  
   

   } 
   
  }); 

}
else
{

$("#availability_status").html('<font color="#cc0000">Username too short</font>');
//if in case the username is less than or equal 3 characters only 
}



return false;
});

});

function logout()
{
	$.ajax({  //Make the Ajax Request
    type: "POST",  
    url: "logout.php",  //file name
    data: "logout=1",  //data
    success: function(server_response){  
	window.location="index.php";

   } 
   
  }); 
	
}

$(document).ready(function(){
  
var nav = $("#nav");
var pos = nav.position();
 $(window).scroll(function() {
 var windowspos = $(window).scrollTop();
 if (windowspos>=pos.top ) {

	nav.addClass('fixedTop');
}
else
{
 nav.removeClass('fixedTop');
}

});

});

</script>



</head>
<body>
<div class="top">
<table width="100%">
<tr>
<td class="top1"><a class="head">
Examination Portal
</a>
</td>
<td>
	<?php if($_SESSION['user']) {echo "Welcome ".$_SESSION['user'].'<br>'; ?>
  <input type="submit" name ="logout" value="logout" onclick="logout();"> <?php } ?>
 </td> 
 </tr>
 </table>
</div>
<div class = "content">
<?php 
function checkdesignation($user)
{
	if($_SESSION['designation'] == $user)
	{
	}
	else
	{
		echo "You do not have sufficient privilege to visit this page";
		include "footer.php";
		die();
	}
}
?>
