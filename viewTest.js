
function change(update,where,what,option)
					{
						what =  $(what).val();
						$("."+where).html('<img src="loader.gif" align="absmiddle">&nbsp;Modifying...');
						
						$.ajax({
									type: "POST",
									data: option+"=" + what+"&id="+where,
									url: "changeQuestions.php",
									cache: false,
									success: function(html){
									$("."+where).html('<img src="available.png" align="absmiddle"> <font color="green">Succesful <br></font>');
									$("."+where).append(html);
									$("."+update).html(what);
												  }
								
								});
				}	
				function changeTest(where,what,option)
					{
						what =  $(what).val();
						$(".changed").html('<img src="loader.gif" align="absmiddle">&nbsp;Modifying...'+option);
						
						$.ajax({
									type: "POST",
									data: option+"=" + what+"&id="+where,
									url: "changeTest.php",
									cache: false,
									success: function(html){
									$(".changed").html('<img src="available.png" align="absmiddle"> <font color="green">Succesful <br></font>');
									$(".changed").append(html);
									$("."+option).html(what);
												  }
								
								});
																		
					
						
						
     
					}
				
				function hide(content)
				{
					$('.'+content).toggle();
					if($("#"+content).val() == "Edit")
						$("#"+content).val("Done");
					else
						$("#"+content).val("Edit");
					
				}
				function time(where)
				{
					hour = $('#h').val();
					minute =$('#m').val();
					second = $('#s').val();
					
					var t = hour+":"+minute+":"+second;
					$("#hiddentime").val(t);
					changeTest(where,'#hiddentime','duration')
					
				}
function adduser(participant)
  {
	  participant =  $(participant).val();
		$(".changed").html('<img src="loader.gif" align="absmiddle">&nbsp;Adding User..'+participant);
		$.ajax({
				type: "POST",
				data: "user=" + participant,
				url: "addparticipants.php",
				cache: false,
				success: function(html){
					
				if(html != 0)
				{
					$(".changed").html('<img src="available.png" align="absmiddle"> <font color="green">Succesful <br></font>');
					$(".changed").append(html);
					$(".user").load("participants.php");
						
				}
				else
				{
					$(".changed").html('<img src="not_available.png" align="absmiddle"> <font color="red">Not Succesful <br></font>');
					$(".changed").append("Sorry the username is already in use");
				}
					
				
							  }
			
			});
	}
function deleteuser(participant)
	{
		$(".changed").html('<img src="loader.gif" align="absmiddle">&nbsp;Deleting User...'+participant);
		$.ajax({
		type: "POST",
		data: "user=" + participant,
		url: "delparticipants.php",
		cache: false,
		success: function(html){
			
		if(html != 0)
		{
			$(".changed").html('<img src="available.png" align="absmiddle"> <font color="green">Succesful <br></font>');
			$(".user").load("participants.php");
			$(".changed").append(html);
		}
		else
		{
			$(".changed").html('<img src="not_available.png" align="absmiddle"> <font color="red">Not Succesful <br></font>');
			
			$(".changed").append("Sorry the delete operation failed");
		}
			
		
					  }
	
	});
	}
	
	function deletequestion(question)
	{
		$("."+question).html('<img src="loader.gif" align="absmiddle">&nbsp;Deleting question...');
		$.ajax({
		type: "POST",
		data: "que=" + question,
		url: "deleteque.php",
		cache: false,
		success: function(html){
			
		if(html != 0)
		{
			$(".question-option").load("question-option.php");
			$(".changed").html('<img src="available.png" align="absmiddle"> <font color="green">Succesful <br></font>');
			$(".changed").append(html);
		}
		else
		{
			$("."+question).html('<img src="not_available.png" align="absmiddle"> <font color="red">Not Succesful <br></font>');
			
			$("."+question).append("Sorry the delete operation failed");
		}
			
		
					  }
	
	});
	}
	function toggleclass(something)
	{
		$('.'+something).toggle();
	}
	function toggleid(something)
	{
		$('#'+something).toggle();
	}

function addque(testid)
{
	
	q = $(".q").val();
	o1 = $(".o1").val();
	o2 = $(".o2").val();
	o3 = $(".o3").val();
	o4 = $(".o4").val();
	o5 = $(".o5").val();
	c = $(".c").val();
	if(q=="" || q== null)
	$(".e5").html("Question can't be empty!!");
	else
	$(".e5").html("");
	if(o1=="" || o1== null)
	$(".e1").html("Option 1 can't be empty!!");
	else
	$(".e1").html("");
	if(o2=="" || o2== null)
	$(".e2").html("Option 2 can't be empty!!");
	else
	$(".e2").html("");
	if(o3=="" || o3== null)
	$(".e3").html("Option 3 can't be empty!!");
	else
	$(".e3").html("");
	if(o4=="" || o4== null)
	$(".e4").html("Option 4 can't be empty!!");
	else
	$(".e4").html("");
	if(q=="" || q== null || o1=="" || o1== null || o2=="" || o2== null || o3=="" || o3== null || o4=="" || o4== null)
	{
	}
	else
	{
		$(".result").html('<img src="loader.gif" align="absmiddle">&nbsp; Adding question...');
			$.ajax({
			type: "POST",
			data: "question=" + q + "&option1=" + o1 + "&option2=" + o2 + "&option3=" + o3 + "&option4=" + o4 + "&option5=" + o5 + "&correct=" + c + "&testid=" + testid,
			url: "queadded.php",
			cache: false,
			success: function(html){
				
			if(html != 0)
			{
				$(".question-option").load("question-option.php");
				$(".result").html('<img src="available.png" align="absmiddle"> <font color="green">Succesful <br></font>');
				$(".result").append(html);
				$(".q").val(""); $(".o1").val("");$(".o2").val("");$(".o3").val("");$(".o4").val("");$(".o5").val("");
			}
			else
			{
				$(".result").html('<img src="not_available.png" align="absmiddle"> <font color="red">Not Succesful <br></font>');
				
				$(".result").append(" <span class='error'>Aha... You didn't add option 5 but selected it as correct option.<br> Question not added. <br> Please try gain</span>");
			}
				
			
						  }
		
					});
	}
}
function addrparticipant(participant)
{
	$(".rrparticipant").html('<img src="loader.gif" align="absmiddle">&nbsp;Adding User..'+participant);
	$.ajax({
				type: "POST",
				data: "user=" + participant,
				url: "addrparticipants.php",
				cache: false,
				success: function(html){
					
				if(html != 0)
				{
					$(".rrparticipant").html('<img src="available.png" align="absmiddle"> <font color="green">Succesful <br></font>');
					$(".rrparticipant").append(html);
					$(".ruser").load("rparticipants.php");
					$(".modal__inner").load("usersinlist.php");
						
				}
				else
				{
					$(".rrparticipant").html('<img src="not_available.png" align="absmiddle"> <font color="red">Not Succesful <br></font>');
					$(".rrparticipant").append("Sorry the username is already in use");
				}
				  }
			});
}
function deleteruser(participant)
	{
		$(".changed").html('<img src="loader.gif" align="absmiddle">&nbsp;Deleting User...'+participant);
		$.ajax({
		type: "POST",
		data: "user=" + participant,
		url: "delrparticipants.php",
		cache: false,
		success: function(html){
			
		if(html != 0)
		{
			$(".changed").html('<img src="available.png" align="absmiddle"> <font color="green">Succesful <br></font>');
			$(".ruser").load("rparticipants.php");
			$(".changed").append(html);
			$(".ruser").load("rparticipants.php");
			$(".add-rp").load("add-rp.php");
		}
		else
		{
			$(".changed").html('<img src="not_available.png" align="absmiddle"> <font color="red">Not Succesful <br></font>');
			$(".changed").append("Sorry the delete operation failed");
		}
			
		
					  }
	
	});
	}
